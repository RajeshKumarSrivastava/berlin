.PHONY: unit lint image/build image/up

ifneq (, $(shell which podman))
CONTAINER_ENGINE := podman
else
CONTAINER_ENGINE := docker
endif

unit:
	PYTHONPATH="$PYTHONPATH:./berlin" poetry run pytest --cov=berlin --cov-report \
		term-missing --cov-fail-under=61 tests/unit

lint:
	poetry run isort .
	poetry run black berlin tests
	poetry run flake8 berlin tests


image/build:
	${CONTAINER_ENGINE} build -t berlin .


image/up: image/build
	${CONTAINER_ENGINE} run --rm -it --network=host -t berlin
