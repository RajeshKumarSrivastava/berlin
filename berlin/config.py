apps = {
    "app_config": {
        "repo_file": "files/package_list.txt",
        "local_diff_file": "files/local_package_list.txt",
    },
    "image_menifest": {"c8s": "", "c9s": ""},
    "image_path": "http://file.rdu.redhat.com/~rasibley/imagebuilder/8.4/5d55aa77-1de5-43c0-ab3e-fe773e5648de-commit.tar",  # noqa
    "cs9_manifest_url": (
        "https://gitlab.com/redhat/automotive/automotive-sig/-/raw/"
        "main/package_list/cs9-image-manifest.lock.json"
    ),
    "dover_submit_endpoint": "http://dover:8080/submit/package_list",
}
transfers = {
    "git_config": {"git_repo_location": ""},
    # "api_url": "http://127.0.0.1:8080",
    "api_url": "http://dover:8080",
    "post_end_point": "/submit/package_list",
}
