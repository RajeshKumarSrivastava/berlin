import json
import logging
from json.decoder import JSONDecodeError
from operator import ne
from typing import Any, Tuple

import make_diff
import pkg_manifest_schema
import requests

import config

logging.basicConfig(level=logging.INFO)


class FileHandler:
    """Calculates diff and
    creates the new file containing updated package list"""

    def __init__(self, src_file: str = ""):
        self._src_file = src_file
        self._repo_pkg_schema_obj = pkg_manifest_schema.PackageManifestSchema()
        self._generated_pkg_schema_obj = (
            pkg_manifest_schema.PackageManifestSchema()
        )
        self._make_diff = make_diff.Diff()

    def get_manifest_file(self, url_config: str) -> str:
        """
        The function pulls the remote json lock_file and returns the string
        object

        Parameters
        ---------
        url_config: str, config key for remote lock_file

        Returns
        ------
        str: string, data object from the remote lock_file
        """
        manifest_data: Any = ""
        url = config.apps[url_config]

        try:
            response = requests.get(url, allow_redirects=True, timeout=(5, 15))
            manifest_data = response.json()
        except requests.exceptions.RequestException as e:
            logging.exception(f"Error in getting the file:{url}, error: {e}")
        except JSONDecodeError as e:
            logging.exception(f"Error in decoding JSON file, error: {e}")

        return manifest_data

    def collect_final_data(self) -> str:
        """
        The function gets the processed and serialized JSON data

        Returns
        ------
        str: string, serialzed JSON data

        """
        final_data = self._repo_pkg_schema_obj.serialize()
        logging.debug(f"Final data to be sent: {json.dumps(final_data)}")
        return json.dumps(final_data)

    def populate_schema_obj(self) -> bool:
        """
        The function gets the deserialized JSON data to python
        structs and keep them as objects which could processed further

        Returns
        ------
        bool: boolean, success/failure of JSON to python deserialization

        """
        repo_lock_file_data = self.deserialize_from_json_text(
            self.get_manifest_file("cs9_manifest_url")
        )
        if not repo_lock_file_data:
            logging.error("Error in deserializing repo lockfile data")
            return False

        self._repo_pkg_schema_obj.deserialize(repo_lock_file_data)

        generated_lock_file_data = self.deserialize_from_json_file(
            self._src_file
        )
        if not generated_lock_file_data:
            logging.error("Error in deserializing generated lockfile data")
            return False
        self._generated_pkg_schema_obj.deserialize(generated_lock_file_data)
        return True

    def deserialize_from_json_file(
        self, file_name: str
    ) -> pkg_manifest_schema.schema:
        """
        The function loads the generate lock_file and deserialize it into
        python structs

        Parameters
        ---------
        file_name: str, file name of locally generated lock_file

        Returns
        ------
        pkg_manifest_schema.schema: schema, alias of the python struct
        """
        with open(file_name) as json_file:
            manifest_data = json.load(json_file)
        file_data: pkg_manifest_schema.schema = manifest_data
        return file_data

    def deserialize_from_json_text(
        self, json_data: str
    ) -> pkg_manifest_schema.schema:
        """
        The function loads the json data from the remote repo lock_file
        and deserialize it into python structs

        Parameters
        ---------
        json_data: str, json string data from the remote lock_file

        Returns
        ------
        pkg_manifest_schema.schema: schema, alias of the python struct
        """
        file_data: pkg_manifest_schema.schema = json_data
        return file_data

    def parse_and_diff(self) -> Tuple[bool, list, bool]:
        """
        The function deserializes the data and calls the diff function to
        perform the diff logic with all the lists of static pinned nvr in
        the repo lock_file and the generated lock_file

        Returns
        ------
        bool: boolean, for the success/failure
        list: list, resultant lock_file data
        bool: boolean, if any of the packages were removed
        """
        if not self.populate_schema_obj():
            logging.error("Error in populating schema objects")
            return False

        result: bool = False
        nvrs_removed: bool = False
        additional_nvr_list: list = list()

        for (atr, repo_nvrs), (k_, generated_nvrs) in zip(
            vars(self._repo_pkg_schema_obj).items(),
            vars(self._generated_pkg_schema_obj).items(),
        ):
            (
                list_changed,
                result_nvr_list,
                additional_nvrs,
                removed_pkg,
            ) = self._make_diff.diff(repo_nvrs, generated_nvrs)
            nvrs_removed |= removed_pkg
            additional_nvr_list.extend(additional_nvrs)
            if list_changed and ne(len(result_nvr_list), 0):
                result_nvr_list.sort()
                setattr(self._repo_pkg_schema_obj, atr, result_nvr_list)
                result = True

        return result, additional_nvr_list, nvrs_removed
