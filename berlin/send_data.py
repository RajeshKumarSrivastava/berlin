import logging

import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder

import config


class SendData:
    """sends the data to a REST service end point"""

    def __init__(
        self, added_nvrs: list = list(), is_nvrs_reduced: bool = False
    ) -> None:
        self._branch_name: str = ""
        self._commit_msg: str = ""
        self._added_nvrs: list = added_nvrs
        self._is_nvrs_reduced: bool = is_nvrs_reduced

    def update_mr_attributes(self):
        """
        The function updates the attributes which are also attributes of the
        module to be sent on the end point

        """
        msg: str = ""
        if len(self._added_nvrs) > 1:
            self._branch_name = (
                f"Updated-packages-{self._added_nvrs[0]}-and-more"
            )
            msg = f"Updated packages {self._added_nvrs[0]} and more"
        if len(self._added_nvrs) == 1:
            msg = self._added_nvrs[0]
            self._branch_name = f"Updated-package-{self._added_nvrs[0]}"
        if self._is_nvrs_reduced:
            msg += ". Some packages were removed"
        self._commit_msg = msg

    def send_parameters(self, data: str, file_name: str) -> bool:
        """
        The function sends the attributes on the communication end point

        Parameters
        ---------
        data: str, result JSON string data for the lock_file
        file_name: str, file name of the repo lock_file

        Returns
        ------
        bool: boolean, success/failure of send
        """
        success: bool = True
        logging.info("Initiating send\n")

        self.update_mr_attributes()

        mp_encoder = MultipartEncoder(
            fields={
                "branch": self._branch_name,
                "commitMessage": self._commit_msg,
                "fileName": file_name,
                "data": data,
            }
        )

        logging.info(
            "Sending data\n branch=%s, commit=%s, file=%s",
            self._branch_name,
            self._commit_msg,
            file_name,
        )

        try:
            response = requests.post(
                config.transfers["api_url"]
                + config.transfers["post_end_point"],
                headers={"Content-Type": mp_encoder.content_type},
                data=mp_encoder,
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            success = False
            logging.error(err.response.text)
        return success
