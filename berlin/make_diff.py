import logging
import re
from operator import eq, lt, ne
from typing import Tuple

logging.basicConfig(level=logging.INFO)


class Diff:
    def __init__(self) -> None:
        pass

    def diff(
        self, repo_nvrs: list, generated_nvrs: list
    ) -> Tuple[bool, list, list, bool]:
        """
        The function calls the diff logic between the repo lock_file and the
        the generated lock_file
        Parameters
        ----------
        repo_nvrs: list, static list of pinned nvr from repo lock_file
        generated_nvrs: list, static list of pinned nvr form generated
                        lock_file

        Returns
        ------
        Tuple[bool, list, list, bool]: bool for the success/failure
                           list, list1 is the result nvrs,
                           list, list2 is the additional nvrs added
                           bool, to specify if nvrs were removed
        """
        success: bool = False
        nvrs_removed: bool = False

        if eq(len(repo_nvrs), 0) or eq(len(generated_nvrs), 0):
            return success, {}, {}, nvrs_removed

        repo_names_set: set = set()
        generated_names_set: set = set()

        generated_name_nvr_dict: dict[str, str] = {}
        repo_name_nvr_dict: dict[str, str] = {}

        result_nvr_list: list = list()
        additional_nvr_list: list = list()

        regex = r"-\d+"
        for repo_nvr in repo_nvrs:
            name = re.split(regex, repo_nvr)[0]
            repo_names_set.add(name)
            repo_name_nvr_dict[name] = repo_nvr
        for generated_nvr in generated_nvrs:
            name = re.split(regex, generated_nvr)[0]
            generated_names_set.add(name)
            generated_name_nvr_dict[name] = generated_nvr

        common_nvrs = list(generated_names_set.intersection(repo_names_set))

        if lt(len(common_nvrs), len(repo_nvrs)):
            nvrs_removed = True
        for nvr in common_nvrs:
            result_nvr_list.append(repo_name_nvr_dict[nvr])

        for nvr in list(generated_names_set.difference(repo_names_set)):
            additional_nvr_list.append(generated_name_nvr_dict[nvr])
            result_nvr_list.append(generated_name_nvr_dict[nvr])
            logging.debug(f"Packages added {additional_nvr_list}")

        if ne(len(result_nvr_list), len(repo_nvrs)):
            success = True

        return success, result_nvr_list, additional_nvr_list, nvrs_removed
