import unittest

import pytest
import responses
import send_data


@pytest.mark.usefixtures("json_data", "nvrs")
@responses.activate
class TestSendData(unittest.TestCase):
    def test_send_message(self):
        sender = send_data.SendData(self.additional_nvr_list)

        responses.add(
            responses.POST,
            url="http://dover:8080/submit/package_list",
            body="Package sent",
        )
        self.assertEquals(
            sender.send_parameters(
                self.json_file_data,
                "cs9-image-manifest.lock.json",
            ),
            200,
        )
