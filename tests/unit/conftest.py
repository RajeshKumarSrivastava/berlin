import pytest


@pytest.fixture(scope="class")
def nvrs(request) -> list:
    class Nvrs:
        repo_nvrs: list = [
            "libsmbclient-4.14.5-2.el9",
            "ModemManager-1.10.8-3.el9",
            "ModemManager-glib-1.10.8-3.el9",
        ]
        generated_nvrs = [
            "libsmbclient-4.14.5-2.el9",
            "ModemManager-1.10.8-3.el9",
            "wget-1.21.1-7.el9",
            "which-2.21-27.el9",
        ]
        result_nvr_list = [
            "ModemManager-1.10.8-3.el9",
            "libsmbclient-4.14.5-2.el9",
            "wget-1.21.1-7.el9",
            "which-2.21-27.el9",
        ]
        additional_nvr_list = ["wget-1.21.1-7.el9", "which-2.21-27.el9"]
        pass

    request.cls.repo_nvrs = Nvrs().repo_nvrs
    request.cls.generated_nvrs = Nvrs().generated_nvrs
    request.cls.result_nvr_list = Nvrs().result_nvr_list
    request.cls.additional_nvr_list = Nvrs().additional_nvr_list


@pytest.fixture(scope="class")
def json_data(request):
    class JSONData:
        json_file_data = (
            '{"cs9":{"common":["abattis-cantarell-fonts-0.0.25-6.el8"'
            '],"arch":{"x86_64":["aspell-0.60.6.1-22.el8"],"aarch64":'
            '["grub2-efi-aa64-2.02-106.el8"]}}}'
        )

        pass

    request.cls.json_file_data = JSONData().json_file_data
