import os
import unittest
from unittest.mock import MagicMock

import pytest

import file_handler


@pytest.mark.usefixtures("nvrs")
class TestFileHandler(unittest.TestCase):
    def test_parse_and_diff(self):
        path = os.path.dirname(
            os.path.join(os.path.dirname(os.path.realpath(__file__)))
        )
        f_handler = file_handler.FileHandler(
            f"{path}/unit/cs9-image-manifest.lock.json"
        )
        f_handler._make_diff.diff = MagicMock(
            return_value=[
                False,
                [],
                self.repo_nvrs,
                False,
            ]
        )

        result, added_nvr_list, nvrs_removed = f_handler.parse_and_diff()
        self.assertFalse(result)
        self.assertEqual(list(dict.fromkeys(added_nvr_list)), self.repo_nvrs)
        self.assertFalse(nvrs_removed)
